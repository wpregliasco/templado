''' templado

'''

import os
from glob import glob
import json
from jinja2 import Environment, FileSystemLoader, DebugUndefined, Template
import jinja2schema
from jinja2schema import infer, to_json_schema
import zipfile

def guess_name(s, paths, exts, template=False):
    '''busca un archivo

       Parameters:
       - s             (str): string to search
       - paths (list of str): paths to search in (sorted)
       - exts  (list of str): exts to look for (can be ['*'])
       - template     (bool): if True and no finds, looks for s_.ext

       Returns:
       - str or None
    '''
    if s is None:
        return None
    s = s.strip()

    out = []
    if '.' in s:
        # con extensión
        for p in paths:
            if p is not None:
                gl = glob(os.path.join(p,s))
                try: 
                    out.append(gl[0])
                except:
                    pass
    else:
        # sin extensión
        for p in paths:
            if p is not None:
                for e in exts:
                    se = f'{s}.{e}'
                    gl = glob(os.path.join(p,se))
                    try: 
                        out.append(gl[0])
                    except:
                        pass
    
    if template and len(out)==0:
        # try with _
        if '.' in s:
            ssplit = s.split('.')
            ext = ssplit[-1]
            ssplit[-2] += '_'
            s_ = '.'.join(s.split('.'))
        else:
            s_ = s + '_'
        return guess_name(s_, paths, exts)

    try:
        return out[0]
    except:
        return None


def guess_output(s, data_str, path_output, path_data):
    '''nombre del archivo de salida

       Parameters:
       - s                   (str): full path of input template
       - data_str            (str): string of instance
       - path_output (str or None): output path (if exists)
       - path_data           (str): for alternative output path

       Primero intenta en path_output
       La salida se completa con _{data_str}

    '''

    # parts of filename
    ssplit = s.split('.')
    sext   = ssplit[-1]
    sbase  = '/'.join(ssplit[-2].split('/')[:-1])
    sname  = ssplit[-2].split('/')[-1]

    # elimino trailing underscore
    if sname.endswith('_'):
        sname = sname[:-1]

    # nombre del file de salida
    sname = f'{sname}_{data_str}'

    # directorio de salida
    if path_output is not None:
        out = f'{path_output}/{sname}.{sext}'
    else:
        out = f'{path_data}/{sname}.{sext}'

    return out
    

class Temple():
    def __init__(self, file_in, file_out, paths):
        '''Parameters
           - file_in (str): pathname of template input
           - file_out(str): pathname of rendered template
           - paths (str): list of indexes paths

        '''

        self.file_in = file_in
        self.file_out= file_out
        self.paths = paths
        self.complete = False
        
        self.in_type = ''
        self.template_str = ''
        self.template = self.load_template(file_in)

        self.missing_keys=[]
        self.rendered = self.render()
        #self.save()


    def template_type(self):
        '''Gives the template type

           template file names can:
           - ends with a type extension (.doc, .md, .txt)
           - ends with .jinja (.doc.jinjam .md.jinja, .txt.jinja)
        '''

        ext = self.file_in.split('.')[-1]
        if ext == 'jinja':
            ext = self.file_in.split('.')[-2]
        
        if ext.lower() in ['odt', 'ods', 'odp']:
            return 'odt'
        elif ext.lower() in ['docx']:
            return 'docx'
        elif ext.lower() in ['xlsx']: 
            return 'xlsx'
        elif ext.lower() in ['md', 'txt', 'html', 'htm', 'pdf', 'svg']:
            return 'txt'
        else:
            print(f"Error: I don't know what to do with a .{ext} file")
            if ext=='pptx':
                print("Convert to Libre Office .odp file.")
            return None
        

    def load_template(self, file):
        '''Loads `file` template from file

           loads in 
           - self.in_type : type of file
           - self.template_str : template string
           - self.template: jinja template object
        '''

        self.in_type = self.template_type()
        type = self.in_type

        path = '/'.join(file.split('/')[:-1])
        fname = file.split('/')[-1]

        if type == 'txt':
            # read content
            with open(file,'r') as f:
                self.template_str = f.read()
            # load content to jinja
            self.jinja_env = Environment(loader=FileSystemLoader(path),
                                         undefined=DebugUndefined)
            return self.jinja_env.get_template(fname)

        elif type == 'odt':
            document = zipfile.ZipFile(file)
            xml_content = document.read('content.xml').decode("utf-8") 
            document.close()
            self.template_str = xml_content
            return Template(xml_content, undefined=DebugUndefined)

        elif type == 'docx':
            document = zipfile.ZipFile(file)
            xml_content = document.read('word/document.xml').decode("utf-8") 
            document.close()
            self.template_str = xml_content
            return Template(xml_content, undefined=DebugUndefined)

        elif type == 'xlsx':
            document = zipfile.ZipFile(file)
            xml_content = document.read('xl/sharedStrings.xml').decode("utf-8") 
            document.close()
            self.template_str = xml_content
            return Template(xml_content, undefined=DebugUndefined)

        else:
            return None


    def assign(self, schema, data, root=''):
        out = {}
        missing_keys = []
        for key in schema.keys():

            if isinstance(schema[key], (str, jinja2schema.model.Scalar)): 
                try:
                    out[key] = data[key]
                except:
                    missing_keys.append(root+key)
                    out[key] = f'{{{{ {root+key} }}}}'

            elif isinstance(schema[key], (dict, jinja2schema.model.Dictionary)):
                out[key]={}
                try:
                    data[key]
                except:
                    data[key] = {}
                out[key], miss = self.assign(schema[key], data[key], root+key+'.')
                missing_keys += miss

        return out, missing_keys

    def load_replacements(self):
        '''load dictionary of field sustitutions

           field syntax: {{file.variable]}}
           files are one of:
            - included files
            - included dicts
        '''
        schema = infer(self.template_str)
        data_files = list(schema.keys())
        
        data = {}
        for file in data_files:
            if isinstance(schema[file], jinja2schema.model.Scalar):
                # include file
                filename = guess_name(file, self.paths, ['md','txt','dat']) 
                try:
                    with open(filename, 'r') as f:
                        data[file] = f.read()
                except: 
                    print(f'\tWarning: Falta archivo de definición "{file}"')

            elif isinstance(schema[file], jinja2schema.model.Dictionary):
                # json
                filename = guess_name(file, self.paths, ['json','dat'])
                try:
                    with open(filename,'r') as f:
                        data[file] = json.load(f)
                except:
                    print(f'\tWarning: Falta archivo de definición "{file}"')

        out, self.missing_keys = self.assign(schema, data, root='')
        return out


    def render(self):

        data = self.load_replacements()
        return self.template.render(**data)


    def zipInsert(self, zip_in, zip_out, fname):
        with zipfile.ZipFile (zip_in, 'r') as zin,\
             zipfile.ZipFile (zip_out, 'w') as zout:

            for item in zin.infolist():
                if item.filename != fname:
                    buffer = zin.read(item.filename)
                    zout.writestr(item, buffer)
                else:
                    buffer = self.render().encode("utf-8")
                    zout.writestr(item, buffer)


    def save(self):
        '''Saves rendered object to file

        '''

        type = self.in_type
        file = self.file_out

        if type == 'txt':
            with open(file, 'w') as f:
                f.write(self.rendered)
        
        elif type == 'odt':
            self.zipInsert(self.file_in, file, 'content.xml')

        elif type == 'docx':
            self.zipInsert(self.file_in, file, 'word/document.xml')

        elif type == 'xlsx':
            self.zipInsert(self.file_in, file, 'xl/sharedStrings.xml')
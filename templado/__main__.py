import sys
from os.path import exists
import os
from .temple import Temple, guess_name, guess_output
from .htmlSA import embedImg


def pathos():
    '''Construye strings de Paths y datos

       Returns:
       - data           (str): string del cwd - para el file de salida
       - path_data      (str): cwd
       - path_templates (str or None): cwd/../templates si existe
       - path_output    (str or None): cwd/../output si existe
    '''

    path_data = os.getcwd()
    data_str = path_data.split('/')[-1]

    path_root = '/'.join(path_data.split('/')[:-1])
    path_templates = path_root + '/templates'
    if not exists(path_templates):
        path_templates = None

    path_output = path_root + '/output'
    if not exists(path_output):
        path_output = None

    return data_str, [path_data, path_templates], path_output


def common_start(s1, s2):
    '''detect common start of strings

    '''
    i = 0
    while s2.startswith(s1[:i]) and i <= len(s1):
        i += 1
    i -= 1
    return s1[i:], s2[i:], s1[:i]


def get_pars(file):
    file_html = '.'.join(file.split('.')[:-1]) + '.html'
    file_notitle = '.'.join(file.split('.')[:-1]) + '-notitle.md'
    with open(file, 'r') as f:
        with open(file_notitle, 'w') as fo:
            title = ''
            for line in f:
                if title == '' and line.strip().startswith('# '):
                    title = line.strip()[2:]
                    if title[-1] == '\n':
                        title = title[:-1].strip()
                elif title != '':
                    fo.write(line)

    return file_html, file_notitle, title


def main(args=None):
    """The main routine."""

    # paths
    data_str, paths_in, path_output = pathos()

    # input files
    if args is None:
        files = sys.argv[1:]

    # convert files
    for fi in files:
        template = guess_name(fi, paths_in, ['md', 'txt', 'odt','*'], template=True)
        if template is None:
            print(f'Error: no sé a qué te referís con "{fi}".')
            continue
        output = guess_output(template, data_str, path_output, paths_in[0])
        temp, outp, _ = common_start(template, output)
        print(temp, '-->', outp)

        # Acá viene el llenado
        t = Temple(template, output, paths_in)
        t.save()

        if t.missing_keys != []:
            print('    Missing keys:')
            print('\t',t.missing_keys)
        else:
            ext = output.split('.')[-1]
            if ext == 'md':
                # conversión a html
                output_html, output_notitle_md, title = get_pars(output)
                cmd = f'pandoc --standalone {output_notitle_md} -o {"temp.html"} '
                cmd += '--resource-path=.  --webtex '
                cmd += '--template=uikit.html --toc '
                cmd += f'--metadata title="{title}"'
                os.system(cmd)

                try:
                    os.remove(output_notitle_md)
                except OSError:
                    pass

                # embed images
                embedImg('temp.html', output_html)

                try:
                    os.remove('temp.html')
                except OSError:
                    pass

                print('    saved as html')

    print('done.')

if __name__ == "__main__":
    sys.exit(main())

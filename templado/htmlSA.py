'''
    HTML Stand Alone maker

    This code replaces the <IMG SRC="..."> 
    by including the base64 representation of the image
    making a standalone version of html
'''

from bs4 import BeautifulSoup
import base64
import mimetypes
import sys

def encodeImage(fileN):
    with open(fileN, "rb") as image_file:
        encoded_img = base64.b64encode(image_file.read())
        mime = mimetypes.guess_type(fileN)[0]
    return mime, encoded_img

def embedImg(fileN, file_out=''):
    # Read soup
    with open(fileN, 'r') as f:
        html = f.read()
    soup = BeautifulSoup(html,'lxml')
    
    # Modify
    for it in soup.find_all('img'):
        src=it.get('src')
        if (not 'data:image' in src) and (not src.startswith('https://')):
            mime, enc = encodeImage(src)
            newsrc = f'data:{mime};base64,{enc.decode("utf-8")}'
            print ('\t',it)
            it['src']=newsrc
            it['alt']=src

    # Save
    if file_out == '':
        file_out = fileN.split('.')
        file_out = '.'.join(file_out[:-1]) + '_SA.' + file_out[-1]

    with open(file_out, "w") as file:
        file.write(str(soup))
        
def main(args):
    if len(args)==1:
        fileN = args[0]
        embedImg(fileN)
        print('done')
        exit(0)
    else:
        print('Usage: htmlFile.html')

if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))


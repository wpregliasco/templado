# ejemplo tomado de https://chriswarrick.com/blog/2014/09/15/python-apps-the-right-way-entry_points-and-scripts/
from setuptools import setup

setup(
    name="my_project",
    version="0.1.0",
    packages=["templado"],
    entry_points={
        "console_scripts": [
            "templado = templado.__main__:main"
        ]
    },
)

# Templator

Sistema simple e ineficiente para completar templates. 

El propósito es armar un reporte automático usando resultados de la corrida de programas externos.

__Nomenclatura__

Se elaborará  un _template_ que contendrá _fields_ para completar con información de _data files_
Eso generará una _instancia_ del template con los datos del _template_ completados para la ocasión.

## WorkFlow

* Se escribirá un template del documento en cualquier formato de texto o en formatos de open office 
  (`.odt`, `.ods`, `.odp`).  
  También implementé `.docx` y `.xlsx` (este último sólo para la primer hoja del workbook).  
  El programa ofrece más opciones para el formato _MarkDown_ (`.md`). 
* Preferentemente los templates tendrán un nombre que termina con un underscore, pero no es obligatorio. 
  Por ejemplo: `miTemplate_.md`
* En el template se incluirán _campos de sustitución_ con el formato `{{ file }}` o `{{ file.variable[.otravariable] }}`
* Como nombre de file tiene que ir sin extensión. Las extensiones las agrega según la regla:
   * en un campo `{{ file }}`, prueba con [`md`,`txt`,`dat`]. El contenido tiene que ser texto.
   * en un campo `{{ file.variable }}`, prueba con [`json`,`dat`] El contenido tiene que ser un json.
  en ambos casos intenta primero en el directorio de la instancia y luego en el del template.
* Si el archivo del template contiene texto, se puede utulizar la sintaxis completa del `jinja` que 
  es superpoderosa. [Consultar documentación](https://jinja.palletsprojects.com/en/2.11.x/templates/)

## Install

```bash
### Dependencias
# pandoc
sudo apt update
sudo apt install pandoc

# easy-pandoc-templates
curl 'https://raw.githubusercontent.com/ryangrose/easy-pandoc-templates/master/copy_templates.sh' | bash

# templado
cd ~/myProjects
git clone git@gitlab.com:wpregliasco/templado.git
cd templado
conda activate myEnv
conda install -c conda-forge beautifulsoup4
conda install -c conda-forge lxml
pip install jinja2schema
pip install -e .
```

En el directorio del programa hay un subdirectorio `images` que hay que copiar en cada directrio de instancia. 
Esta es una porquería que ya iremos arreglando.

Con esto tenemos el comando `templado` disponible en cualquier path del sistema (siempre dentro de nuestro environment).


## Estructura de los directorios

Según la complejidad del proyecto, se pueden armar diferentes configuraciones.

### proyecto simple

Ponemos todo en un directorio, 

```
docs
├── myTemplate_.md
├── myData.md
└── myDict.json
```

nos paramos en le directorio `docs` y corremos el comando: `templado myTemplate`

esto genera un archivo `myTemplate_docs.md` con los datos completados.

### proyecto no tan simple

```
docs
├── templates                     <- usar exactamente este nombre
│   └── myTemplate_.md            <- template
├── output                        <- usar exactamente este nombre
│   ├── myTemplate_instancia1.md  <- instancia generada con templado
│   └── myTemplate_instancia2.md  <- instancia generada con templado
├── instancia1
|   └── myData.md                 <-datos de la instancia 1
└── instancia2
    └── myData.md                 <-datos de la instancia 2
```
nos paramos en el directorio de la instancia y corremos el comando: `templado myTemplate`
lo cual genera la salida en el directorio de `output`. Si este directorio no existe, la salida queda en
el directorio de cada instancia, lo cual también es una opción interesante. 

## comando `templado`

La sintaxis del comando es

```
$ templado myTemplate1 myTemplate2 ...
```

Lo que hará es instanciar los templates en orden sucesivo.
